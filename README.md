# Travail pratique 3

Dans ce troisième travail pratique, vous devez concevoir un jeu vidéo de
[plateformes](https://en.wikipedia.org/wiki/Platform_game) (en anglais,
*platformer*) à l'aide de la bibliothèque [SDL2](https://www.libsdl.org/).

Notez que ce travail ne peut pas être réalisé sur les serveurs Malt/Java, qui
supportent seulement les applications sur la console et pas les applications
graphiques.

Le travail peut être réalisé **seul** ou en **équipe** d'au plus **3
personnes**. Il doit être remis au plus tard le **30 décembre 2017** à
**23h59**. À partir de minuit, une pénalité de **2% par heure** de retard sera
appliquée.

**Attention**. Nous nous réservons la possibilité d'attribuer une note
complètement différente (pouvant aller jusqu'à zéro) à un membre d'une équipe
dont le travail n'est pas jugé suffisant. Pour cela, nous étudierons la
division des tâches et les *commits* (quantité et qualité) qui ont été produits
par chacun des membres (Voir le phénomène du [passager clandestin](https://fr.wikipedia.org/wiki/Passager_clandestin_(%C3%A9conomie))).

## Objectifs spécifiques

Les principaux objectifs visés sont les suivants :

- **Construire** un logiciel fonctionnel en C à partir de zéro;
- Utiliser correctement un **logiciel de contrôle de version** pour
  **collaborer** (travailler en équipe) et **structurer** le développement (en
  équipe ou seul);
- Se familiariser avec une **bibliothèque C graphique**;
- **Livrer** une application en facilitant sa **distribution** et son
  **utilisation**.

## Description du travail

Un jeu de plateformes est une application graphique interactive dans laquelle
des personnages ou des entités évoluent et interagissent dans un environnement
à base de plateformes. La franchise de jeu de plateformes la plus connue est
sans aucun doute Mario Bros.

Il vous est demandé de créer un petit jeu de plateformes très simple dans
lequel un personnage (que j'appelle **Bob**) se déplace. Bob est très limité
dans ses actions. Il ne peut être que dans un des états suivants:

- Il peut être **inactif** (*idle* en anglais);
- Il peut **marcher** vers la **gauche**;
- Il peut **marcher** vers la **droite**;
- Il peut **sauter verticalement**;
- Il peut **sauter** vers la **gauche**;
- Il peut **sauter** vers la **droite**.

Vous devez minimalement implémenter ces six actions, mais vous êtes libres
d'ajouter des actions supplémentaires si vous le souhaitez.

Par ailleurs, les scènes dans lesquelles Bob se déplacent ont une géométrie
simple. Elles sont constituées uniquement de **rectangles**, qui sont orientés
de façon **horizontale** (**plateformes**) ou **verticale** (**murs**).
Évidemment, Bob ne peut passer ni à travers les murs ni à travers les
plateformes. Voici une image illustrant un tel niveau (vous êtes libres
d'utiliser d'autres couleurs et des plateformes plus jolies, ou simplement des
rectangles).

![](images/niveau.png)

Vous remarquerez que des **beignes** apparaissent à différents endroits dans la
scène. La raison pour cela est que Bob adore les beignes. Son objectif consiste
donc à se balader dans le niveau et à tous les récupérer (c'est la condition
gagnante pour terminer le niveau). Il est donc impossible de perdre à ce jeu,
mais vous êtes libres d'ajouter un chronomètre décroissant qui impose une
limite de temps pour récupérer les beignes si vous voulez rendre le jeu un peu
plus intéressant.

À noter que la conception d'un jeu de plateformes requiert la maîtrise de
certaines notions théoriques sur les applications graphiques, la cinématique et
les automates finis (aussi appelés [machines à états finies](https://en.wikipedia.org/wiki/Finite-state_machine)),
détaillées un peu plus bas.

## Applications graphiques

Le développement d'applications graphiques interactives se distingue des
applications consoles à plusieurs niveau :

- Il repose sur l'utilisation de fichiers multimédia comme des **images**, des
  **fichiers audio** ou encore des **fichier vidéos**;
- Il doit tenir compte des interventions de l'utilisateur via divers
  périphériques, comme le **clavier**, la **souris**, les **écrans tactiles**,
  etc.
- Il doit gérer différents événements en fonction du **temps**, en utilisant
  notamment des **chronomètres**.
- Il repose souvent sur l'utilisation de **feuilles de sprites** (en anglais,
  *spritesheets*), une matrice de plusieurs petites images regroupées en une
  seule pour simuler l'animation d'un objet.

En particulier, la bibliothèque SDL est une bibliothèque de bas niveau, en
comparaison par exemple à Swing (Java). Elle n'offre pas de système de boutons,
de menu déroulant, de boutons radio, de gestion d'événements par défaut, etc.

Dans ce travail pratique, il est donc recommandé de simplifier au maximum cette
gestion si vous souhaitez compléter le projet dans le temps imparti. Voici
quelques suggestions (vous êtes évidemment libres de ne pas en tenir compte):

- N'utilisez que les événements **clavier** de l'utilisateur (pas d'événement
  **souris**, sauf quand on clique sur le `x` qui ferme la fenêtre du jeu);
- Utilisez les **ressources graphiques** que je vous fournis (mais n'hésitez
  pas à les modifier ou à en ajouter si vous êtes motivés, à la toute fin);
- Assurez-vous d'abord d'avoir une version fonctionnelle dans laquelle les
  images sont **statiques** (ou même des **rectangles**) pour ensuite les
  remplacer par des images animées.
- N'ajoutez du **son** et/ou de la **musique** que si vous avez terminé toutes
  les fonctionnalités demandées (ce n'est pas évalué).

Une bonne façon de comprendre le fonctionnement de la bibliothèque SDL est
d'étudier le [jeu Maze](https://bitbucket.org/ablondin-projects/maze-sdl).
Quelques remarques s'imposent par rapport à cette petite application:

- Dans ce travail pratique, vous n'aurez **pas besoin** des bibliothèques TMX
  et XML sur lesquelles est basée le jeu. Par contre, pour faire tourner le jeu
  Maze, vous devez installer TMX/XML.
- Il y a un module qui permet de gérer les **feuilles de sprites**. N'hésitez
  pas à le récupérer et à l'adapter à vos besoins (sans oublier de citer la
  source!).
- Étudiez soigneusement la façon d'implémenter les **machines à états** dans
  Maze, qui sont des patrons fondamentaux dans le développement d'applications
  graphiques (plus de détails dans une sous-section plus bas).

## Ressources graphiques

Toutes les ressources graphiques nécessaires pour le projet (sauf les murs et
les plateformes, que je vous laisse concevoir) peuvent être téléchargées dans
le dépôt https://gitlab.com/ablondin/inf3135-aut2017-tp3-assets. Il n'y a en
fait que deux feuilles de sprites, une première pour Bob :

![](images/character.gif)

À noter que chaque ligne correspond à une action (marcher vers la droite,
marcher vers la gauche, être inactif, sauter verticalement, sauter vers la
droite et sauter vers la gauche). Chaque petite image est de dimensions 128x128
(en pixels).

Une seconde feuille de sprites est utilisée pour le beigne :

![](images/donut.gif)

Dans ce cas, il n'y a qu'une seule action possible, qui est de tourner sur
lui-même. Ici aussi, chaque petite image est de dimensions 128x128 (toujours en
pixels).

Vous pouvez inclure les ressources graphiques de différentes façon dans votre
dépôt:

- En ajoutant directement les fichiers PNG correspondant dans un dossier nommé
  `assets` (ou un autre nom de votre choix);
- En démarrant votre projet comme un clone du dépôt qui contient les ressources
  (même s'il y a peu de chance qu'il soit modifié).
- En l'ajoutant comme
  [sous-module](https://git-scm.com/book/fr/v2/Utilitaires-Git-Sous-modules).
  C'est sans doute la façon la plus propre de le faire parmi les trois, mais
  les trois façons de faire sont tout à fait acceptables.

## Automates

Dans les applications graphiques en général, la notion d'*état* est
primordiale. Le formalisme sans doute le plus utilisé pour modéliser les états
d'un système est l'**automate fini**, aussi appelé **machine à transition** ou
**machine à états**. Il s'agit essentiellement d'un **graphe orienté**,
c'est-à-dire un ensemble de **noeuds** ou **sommets**, qui représentent les
différents états possibles et d'un ensemble d'**arcs** (aussi appelés
**flèches** ou **transitions**), qui représentent les règles pour passer d'un
état à un autre.

Voici un exemple d'automate illustrant les états possibles du jeu que je vous
demande de développer (vous pouvez évidemment en utiliser un autre):

![](images/game-states.png)

Il y a donc 7 états possibles (les ellipses en bleu pâle). Les règles pour
passer d'un état à l'autre sont représentées par des flèches rouges. Par
exemple, lorsque le joueur est en train de jouer (état `Playing`), il peut y
avoir deux façons de quitter cet état: en ayant terminé d'attraper tous les
beignes de la scène (`ALL_DONUTS_COLLECTED`) ou lorsque le temps s'est écoulé
(`TIME_OUT`), en supposant que vous ayez mis un chronomètre dans votre jeu.

À noter que ces constructions ne sont pas directement implémentées dans le code
source sous cette forme, mais elles aident à organiser les modules et la
logique du fonctionnement.

La même idée s'applique pour le personnage de Bob, qui pourrait par exemple
être régi par l'automate ci-bas:

![](images/bob-states.png)

Vous pourriez modéliser la situation, par exemple, en définissant votre
personnage à l'aide du type énumératif et de la structure suivante
(incomplète):

```c
enum BobState {
    BOB_IDLE               = 0, // Bob is idle
    BOB_WALKING_RIGHT      = 1, // Bob is walking to the right
    BOB_WALKING_LEFT       = 2, // Bob is walking to the left
    BOB_JUMPING_VERTICALLY = 3, // Bob is jumping vertically
    BOB_JUMPING_RIGHT      = 4, // Bob is jumping to the right
    BOB_JUMPING_LEFT       = 5, // Bob is jumping to the left
};

struct Bob {
    enum BobState state; // Bob's current state
    unsigned int frame;  // Bob's current frame
    float x;             // Bob's x position
    float y;             // Bob's y position
    float vx;            // Bob's x-coordinate velocity
    float vy;            // Bob's y-coordinate velocity
};
```

Ainsi, pour dessiner correctement Bob, il suffit simplement de connaître son
état courant, l'image courante (en anglais, *frame*) qui est un nombre entre 0
et 31 (puisqu'il y a 32 images par action), sa position sur l'écran `(x,y)` et
sa vitesse `(vx, vy)`.

## Cinématique

Lorsque des entités évoluent dans des environnements 2D, elles sont souvent
affectées par des forces physique. Dans le cas qui nous occupe, il n'y a que
deux types de force:

- La force **gravitationnelle**, qui est constante et qui fait en sorte que Bob
  est toujours attiré vers le sol;
- La force de résistance des **plateformes** et des **murs** qui fait en sorte
  que Bob ne puisse pas passer au travers.

Pour implémenter ces deux contraintes, il vous suffit de prendre en compte la
**vélocité** du personnage, c'est-à-dire le vecteur `(vx, vy)` représentant sa
vitesse courante. Vous n'aurez qu'à mettre à jour la vélocité au fur et à
mesure que le temps s'écoule pour tenir compte de l'effet de la gravité.

Quant aux collisions avec les plateformes et les murs, il vous suffira de
vérifier que la position courante de votre personnage n'est pas située à
l'intérieur du rectangle correspondant. Ce n'est pas très difficile à gérer et
vous pouvez facilement trouver des explications détaillées sur le sujet, par
exemple ce [tutoriel en français](http://loka.developpez.com/tutoriel/sdl/collision/).

## Makefile et tests automatiques

Lors de la remise, vous devez inclure un fichier `Makefile` permettant de
compiler votre projet à l'aide de la commande

```sh
make
```

**Attention**, si vous travaillez sur MacOS, n'utilisez pas les *Frameworks*
pour SDL, qui ne sont pas facilement portables. Utilisez plutôt le gestionnaire
de paquets MacPorts qui fait une installation similaire à Aptitude sur Ubuntu
et Mint. En particulier, les travaux ne seront pas corrigés sur MacoS, donc il
est important qu'ils soient compilables sur une distribution Linux.

Par ailleurs, bien qu'il n'y ait pas vraiment de façon simple de tester de
façon automatique une application graphique (étant donné qu'elle demande une
participation interactive de la part d'un utilisateur), vous devez minimalement
ajouter un fichier `.gitlab-ci.yml` qui garantit que le projet compile
correctement à chaque fois que quelqu'un **pousse** une modification (bref, le
seul test est que le projet compile).  Un tel fichier ressemblerait à ceci (il
est possible que vous ayez à le modifier):

```yaml
before_script:
    - apt-get update -qq
    - apt-get install -y build-essential
    - apt-get install -y libsdl2-dev
    - apt-get install -y libsdl2-gfx-dev
    - apt-get install -y libsdl2-image-dev
    - apt-get install -y libsdl2-ttf-dev

build:
    stage: build
    script:
        - make
```

## Fichier README

Vous devez inclure un fichier README avec votre projet afin de documenter
différents aspects de celui-ci. Vous devez minimalement inclure les éléments
suivants, mais n'hésitez pas à en ajouter si nécessaire.

```md
# Travail pratique 3

## Description

Description du projet en quelques phrases.
Mentionner le contexte (cours, sigle, université, etc.).

## Auteurs

- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)
- Prénom et nom (Code permanent)

## Fonctionnement

Expliquez comment fonctionne le jeu, quel est son but, quelles sont les
touches permettant de déplacer le personnage, etc.

## Plateformes supportées

Indiquez toutes les plateformes sur lesquelles vous avez testé
l'application, avec la version.

## Dépendances

Décrivez toutes les dépendances de votre projet, autant au niveau des
logiciels (s'il y en a) que des bibliothèques (par exemple SDL2).
Fournissez les liens vers les sites officiels des dépendances si possible.

## Compilation

Expliquez comment compiler et lancer l'application.

## Références

Citez vos sources ici, s'il y a lieu.

## Division des tâches

Donnez ici une liste des tâches de chacun des membres de l'équipe. Utilisez
la syntaxe suivante (les crochets vides indiques que la tâche n'est pas
complétée, les crochets avec un `X` indique que la tâche est complétée):

- [ ] Programmation du menu principal (Alice)
- [X] Conception des plateformes (Charlie)
- [ ] Gestion des collisions (Charlie)
- [ ] Gestion des feuilles de sprites (Alice)
- [ ] Ajout de sons (Charlie)

  - [ ] lorsque le personnage saute;
  - [ ] lorsque le personnage atterrit;
  - [X] lorsque le personnage prend un beigne;

## Statut

Indiquer si le projet est complété et sans bogue. Sinon, expliquez ce qui
manque ou ce qui ne fonctionne pas.
```

## Contraintes additionnelles

Votre jeu doit être complet et fonctionnel. Il doit minimalement inclure

- un **menu** de démarrage;
- un **niveau** (il n'est pas nécessaire d'en faire plusieurs);
- un **écran** indiquant que le niveau est terminé (cet écran peut apparaître
  dans le niveau réussi ou sur un écran différent).

En outre, afin d'éviter des pénalités, il est important de respecter les
contraintes suivantes :

- Le nom de votre projet doit être exactement `inf3135-aut2017-tp3`.
- L'URL de votre projet doit être exactement
  `https://gitlab.com/<nom d'utilisateur>/inf3135-aut2017-tp3`, où
  `<nom d'utilisateur>` est l'identifiant GitLab d'un membre de l'équipe.
- **Aucune variable globale** (à l'exception des constantes) n'est permise;
- Vos modifications doivent **compiler** sans **erreur** et sans
  **avertissement** lorsqu'on entre `make`.
- Vous devez donner accès en mode *Developer* aux utilisateurs `ablondin` et
  `Morriar`, peu importe que vous soyez dans le groupe 20 ou le groupe 40 (les
  travaux seront corrigés par les deux enseignants).

Advenant le non-respect d'une ou plusieurs de ces contraintes, une pénalité de
**50%** sera appliquée.

## Remise

Votre travail doit être remis au plus tard le **30 décembre 2017** à **23h59**.
À partir de minuit, une pénalité de **2% par heure** de retard sera appliquée.

La remise se fait **obligatoirement** par l'intermédiaire de la plateforme
[GitLab](https://about.gitlab.com/>).  **Aucune remise par courriel ne sera
acceptée** (le travail sera considéré comme non remis).

Les travaux ne pourront pas être corrigés sur le serveur Malt, qui ne permet
pas d'exécuter une application graphique. Vous devez cependant vous assurer que
votre programme fonctionne **sans modification** sur au moins une distribution
Linux telle que Ubuntu ou Mint. Aucun travail ne sera corrigé sur **Windows**
ou **MacOS**.

## Barème

Votre travail sera évalué selon les critères de correction suivants (sur un
total de **200 points**):

- Fonctionnabilité: **120 points**.

  * Gestion des états du jeu et du personnage: **30 points**;
  * Gestion des feuilles de sprites: **20 points**;
  * Gestion de la physique: **30 points**;
  * Détection des collisions: **20 points**;
  * Interaction avec l'environnement: **20 points**.

- Distribution: **40 points**.

  * Compilation simple (`Makefile`): **10 points**.
  * Documentation du fonctionnement (`README`): **30 points**.

- Gestion du projet: **40 points**.

  * La grande majorité des modifications devraient être ajoutées par **requêtes
    d'intégration**, afin de bien structurer le développement. Notez que le
    texte qui explique la requête d'intégration n'a pas à être aussi détaillé
    que pour le 2e travail pratique, une courte explication étant suffisante.
  * Si vous êtes en **équipe**, vous pouvez désigner un membre unique de
    l'équipe comme celui qui accepte ou refuse les requêtes d'intégration (il
    joue le rôle du *release manager*). Il est aussi acceptable d'intégrer une
    requête d'intégration dès qu'au moins deux personnes différentes l'ont
    évaluée.
  * Si vous travaillez **seul**, vous devez tout de même structurer vos
    modifications à l'aide de requêtes d'intégration, que vous accepterez
    vous-même. C'est sûr que c'est un peu plus artificiel, mais ça permet au
    moins de bien étudier, rétroactivement, le développement de votre
    programme. En particulier, assurez-vous de laisser toutes les branches que
    vous avez utilisées et fusionnées sur le dépôt afin qu'on puisse les
    étudier si nécessaire.
